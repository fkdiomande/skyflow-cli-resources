<div align="center">
    <a href="https://skyflow.io">
        <img width="150" height="150" src="assets/logos/skyflow/skyflow.svg">
    </a>
    <br>
    <br>

[![Skyflow website](assets/badges/site.svg)][website-url]
[![Join the chat](assets/badges/slack.svg)][slack-url]

</div>

<h1 align="center">Skyflow CLI Resources</h1>


## What is Skyflow CLI Resources ?

**Skyflow CLI Resources** is a community project, which aims to bring together a community of **developers** who use **Skyflow CLI** around the documentation and the customization of our API data.


This project is composed by 2 folders. The first one, named **"api"** contains the data that are used by Skyflow-cli when you work with it on your computer.


The second one is the folder **"doc"**, where you can find all of the documentation displayed on our <a href="https://skyflow.io/" target="blank">Website</a>.
```
https://skyflow.io
```

## How to contribute?

You are free to modifie our project's files and upgrade them.


If you need help to participate in our project, you can see our tutorials about:

- **<a href="/api/compose/README.md" target="blank">Compose</a>**
- **<a href="/api/package/README.md" target="blank">Package</a>** 
- **<a href="/doc/commands/README.md" target="blank">Commands</a>** 
- **<a href="/doc/modules/README.md" target="blank">Modules</a>** 

## Contact

You can contact us through **GitHub** or [Slack][slack-url] in our public workspace at 
```
skyflow-cli.slack.com
```

**Thanks you** for your participation in our project.<br/>
The Skyflow CLI team.

[website-url]: https://skyflow.io
[slack-url]: https://join.slack.com/t/skyflow-cli/shared_invite/enQtNDg4MDIyODQ3Njg0LWYwMTUxZGM3NmQ3MGJhZTA3MDAzNTcwYWM2MzFjNzZmNzAzOWUxZjQ1YTkwMjVkNzU1NjUyMmM2Yjc1ZDI3NzQ