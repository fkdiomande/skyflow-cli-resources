# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 


Please note we have a code of conduct, please follow it in all your interactions with the project.

## Welcome contributors to the project

### How to contribute ?

You are free to modifie our project's files and upgrade them.


Don't forget to read our **<a href="/README.md" target="blank">README</a>**, you will find informations about the good ways of contributing.

### How to submit changes ?

You're free to update our files, bit we have a **Pull Request Template**, please respect it.

### How to report a bug ?

You can report bugs by Github, please be sure to respect our **Issue Template**.

## Code of Conduct

We have a Code of Conduct, read it **<a href="/CODE_OF_CONDUCT.md" target="blank">Here</a>**

## Contact

You can contact us through **GitHub** or [Slack][slack-url] in our public workspace at 
```
skyflow-cli.slack.com
```

**Thanks you** for your participation in our project.<br/>
The Skyflow CLI team.