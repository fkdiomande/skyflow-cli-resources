/**
 * Select object with a search field.
 *
 * @class Select
 * @constructor
 * @author Skyflow
 * @version 1.0.0
 * @requires Helper
 */
class Select {

    /**
     * Constructor.
     *
     * @method constructor
     * @param {Object|Array} data Data for select object.
     * @param {String|HTMLElement} container Container of select object. Must be an element from the DOM.
     * @param {Object} config
     * @returns {Select} Returns a new instance of Select object.
     * @since 1.0.0
     */
    constructor(data, container, config) {

        if (!Helper.isObject(config)) {
            config = {};
        }

        /**
         * Default configuration array.
         *
         * @property config
         * @type Object
         * @default
         *      {
         *          search: true,
         *          multiple: false,
         *          events: {
         *              show: null,
         *              hide: null,
         *              select: null,
         *              unselect: null,
         *              search: null,
         *          },
         *      }
         *
         * @since 1.0.0
         */
        this.config = {
            search: true,
            multiple: false,
            events: {
                show: null,
                hide: null,
                select: null,
                unselect: null,
                search: null,
            },
        };
        if(Helper.isObject(config)){
            this.config = Object.assign({}, this.config, config);
        }

        /**
         * Container of select object. Must be an element from the DOM.
         *
         * @property container
         * @type String|HTMLElement
         * @since 1.0.0
         */
        this.container = null;

        if (Helper.isString(container)) {
            this.container = document.querySelector(container);
        }

        if (Helper.isElement(container)) {
            this.container = container;
        }

        /**
         * Data for select object.
         *
         * @property data
         * @type Object|Array
         * @since 1.0.0
         * @example
         *      const select = new Select();
         *      select.data = {
         *          'key1': 'value1',
         *          'key2': 'value2'
         *      }
         */
        this.data = data;

        /**
         * Data keys for select object.
         *
         * @property keys
         * @type Array
         * @since 1.0.0
         */
        this.keys = Object.keys(data);

        /**
         * Data values for select object.
         *
         * @property values
         * @type Array
         * @since 1.0.0
         */
        this.values = Object.values(data);

        /**
         * Search element.
         *
         * @property searchElement
         * @type HTMLElement
         * @default HTMLInputElement
         * @since 1.0.0
         */
        this.searchElement = document.createElement('input');

        /**
         * Select element.
         *
         * @property selectElement
         * @type HTMLElement
         * @default HTMLUlElement
         * @since 1.0.0
         */
        this.selectElement = document.createElement('ul');

        /**
         * Item element.
         *
         * @property itemElement
         * @type String
         * @default 'li'
         * @since 1.0.0
         */
        this.itemElement = 'li';

        /**
         * List of items.
         *
         * @property items
         * @type HTMLElement[]
         * @since 1.0.0
         */
        this.items = [];

        /**
         * Selected element. Stores selected elements.
         *
         * @property selected
         * @type HTMLElement|HTMLElement[]
         * @default false|[]
         * @since 1.0.0
         */
        this.selected = this.config.multiple ? [] : false;

        /**
         * Search values.
         *
         * @property searchValues
         * @type HTMLElement[]
         * @default false|[]
         * @since 1.0.0
         */
        this.searchValues = [];

        this.searchElement.style.display = 'none';

        this.searchElement.addEventListener('input', (e) => {
            const value = e.target.value.toLowerCase();

            this.searchValues = [];

            this.items.map((item) => {
                if (
                    item.dataset.value.toLowerCase().indexOf(value) > -1
                    || item.textContent.toLowerCase().indexOf(value) > -1
                ) {
                    item.style.display = 'block';
                    this.searchValues.push(item);
                } else {
                    item.style.display = 'none';
                }
            });

            if (Helper.isCallback(this.config.events.search)) {
                this.config.events.search.apply(null, [this]);
            }
        }, false);

        this.selectElement.style.display = 'none';

        // Manage list items
        this.keys.map((key) => {
            const option = document.createElement(this.itemElement);
            option.dataset.selected = 0;
            option.addEventListener('click', (e) => {
                const isSelected = Math.abs(parseInt(e.target.dataset.selected, 10) - 1);
                e.target.dataset.selected = isSelected;
                if (this.config.multiple) {
                    let selectedItems = this.selectElement.querySelectorAll("[data-selected='1']");
                    selectedItems = Helper.convertToArray(selectedItems);
                    this.selected = selectedItems;
                } else {
                    if (this.selected) {
                        this.selected.dataset.selected = '0';
                    }
                    this.selected = this.selectElement.querySelector("[data-selected='1']") || false;
                    if (this.selected) {
                        this.selected.dataset.selected = '1';
                    }
                }

                if (isSelected === 1 && Helper.isCallback(this.config.events.select)) {
                    this.config.events.select.apply(null, [this]);
                }

                if (isSelected === 0 && Helper.isCallback(this.config.events.unselect)) {
                    this.config.events.unselect.apply(null, [this]);
                }
            }, false);
            option.dataset.key = key;
            option.innerHTML = this.data[key];
            this.selectElement.appendChild(option);
            this.items.push(option);
            return value;
        });

        if (Helper.isElement(this.container)) {
            this.container.appendChild(this.searchElement);
            this.container.appendChild(this.selectElement);
        }

        return this
    }

    /**
     * Enabled or disabled search element.
     *
     * @method search
     * @param {Boolean} search Set true to enable and false to disable.
     * @returns {Select} Returns the same instance of the object.
     * @since 1.0.0
     * @returns {Select} Returns the current Select object.
     */
    search(search = true) {
        if (!Helper.isBoolean(search)) {
            return this;
        }

        this.searchElement.style.display = (search ? 'block' : 'none');
        this.config.search = search;

        return this;
    }

    /**
     * Show select object.
     *
     * @method show
     * @returns {Select} Returns the same instance of the object.
     * @since 1.0.0
     * @returns {Select} Returns the current Select object.
     */
    show() {
        this.selectElement.style.display = 'block';
        this.search(this.config.search);
        if (Helper.isCallback(this.config.events.show)) {
            this.config.events.show.apply(null, [this]);
        }
        return this;
    }

    /**
     * Hide select object.
     *
     * @method hide
     * @returns {Select} Returns the same instance of the object.
     * @since 1.0.0
     * @returns {Select} Returns the current Select object.
     */
    hide() {
        this.selectElement.style.display = 'none';
        this.search.style.display = 'none';
        if (Helper.isCallback(this.config.events.hide)) {
            this.config.events.hide.apply(null, [this]);
        }
        return this;
    }

    /**
     * Set events for select object.
     *
     * @method on
     * @param {String} event Event name.
     * @param {Function} callback Function to trigger.
     * @returns {Select} Returns the same instance of the object.
     * @since 1.0.0
     * @returns {Select} Returns the current Select object.
     */
    on(event, callback) {
        if (
            Object.prototype.hasOwnProperty.call(this.config.events, event)
            && Helper.isCallback(callback)
        ) {
            this.config.events[event] = callback;
        }

        return this;
    }

}