/**
 * Various useful functions.
 *
 * @class Helper
 * @static
 * @author Skyflow
 * @version 1.0.0
 * @example
 *      Heller.isElement(document.createElement('div')); // Returns true
 *      Heller.isObject({}); // Returns true
 */
class Helper {

    /**
     * Get type of any object.
     *
     * @method getType
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {String} Returns the type of object.
     */
    static getType(object) {
        if (object === null) {
            return null
        }
        let t = (typeof object);
        if (t === 'object') {
            object = String(object.constructor);
            if (/^(?:function|object) ([a-z0-9-]+)\(?/i.test(object)) {
                t = RegExp.$1;
                if (/^html[a-z]*element$/i.test(t)) {
                    t = 'Element'
                }
            } else {
                t = undefined
            }
        }

        return t;
    }

    /**
     * Check if an object is a string.
     *
     * @method isString
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a string and false otherwise.
     */
    static isString(object) {
        return this.getType(object) === 'string';
    }

    /**
     * Check if an object is a number.
     *
     * @method isNumber
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a number and false otherwise.
     */
    static isNumber(object) {
        return this.getType(object) === 'number';
    }

    /**
     * Check if an object is a array.
     *
     * @method isArray
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a array and false otherwise.
     */
    static isArray(object) {
        return this.getType(object) === 'Array';
    }

    /**
     * Check if an object is a object.
     *
     * @method isObject
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a object and false otherwise.
     */
    static isObject(object) {
        return this.getType(object) === 'Object';
    }

    /**
     * Check if an object is a boolean.
     *
     * @method isBoolean
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a boolean and false otherwise.
     */
    static isBoolean(object) {
        return this.getType(object) === 'boolean';
    }

    /**
     * Check if an object is a DOM element.
     *
     * @method isElement
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a DOM element and false otherwise.
     */
    static isElement(object) {
        return getType(object) === 'Element';
    }

    /**
     * Check if an object is a function.
     *
     * @method isFunction
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a function and false otherwise.
     */
    static isFunction(object) {
        return this.getType(object) === 'function';
    }

    /**
     * Check if an object is a function.
     *
     * @method isCallback
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is a function and false otherwise.
     */
    static isCallback(object) {
        return this.isFunction(object);
    }

    /**
     * Check if an object is empty.
     *
     * @method isEmpty
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true for empty array, string, object, false, undefined, 0, null, NaN and false otherwise.
     */
    static isEmpty(object) {
        if (!object) {
            return true
        }
        for (let k in object) {
            if (object.hasOwnProperty(k)) {
                return false
            }
        }

        if (object === true || this.isNumber(object)) {
            return false;
        }

        return true;
    }

    /**
     * Check if an object is null.
     *
     * @method isNull
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is null and false otherwise.
     */
    static isNull(object) {
        return object === null;
    }

    /**
     * Check if an object is false.
     *
     * @method isFalse
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is false and false otherwise.
     */
    static isFalse(object) {
        return object === false;
    }

    /**
     * Check if an object is true.
     *
     * @method isTrue
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is true and false otherwise.
     */
    static isTrue(object) {
        return object === true;
    }

    /**
     * Check if a string is blank.
     *
     * @method isBlank
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the string is blank and false otherwise.
     */
    static isBlank(object) {
        return this.isString(object) && object.trim() === '';
    }

    /**
     * Check if an object is regular expression.
     *
     * @method isRegExp
     * @param object Object we want to know the type.
     * @since 1.0.0
     * @returns {Boolean} Returns true if the object is regular expression and false otherwise.
     */
    static isRegExp(object) {
        return this.getType(object) === 'RegExp';
    }

    /**
     * Convert an object to array.
     *
     * @method convertToArray
     * @param object Object to convert.
     * @since 1.0.0
     * @returns {Array} Returns the resulting array.
     */
    static convertToArray(object) {

        if (this.isObject(object)) {
            return Object.keys(object);
        }

        return [].slice.call(object);

    }

    /**
     * Check if platform is windows.
     *
     * @method isWindows
     * @since 1.0.0
     * @returns {Boolean} Returns true if the platform is windows and false otherwise.
     */
    static isWindows(){
        return process.platform === 'win32';
    }

    /**
     * Check if platform is linux.
     *
     * @method isLinux
     * @since 1.0.0
     * @returns {Boolean} Returns true if the platform is linux and false otherwise.
     */
    static isLinux(){
        return process.platform === 'linux';
    }

    /**
     * Check if platform is mac.
     *
     * @method isMac
     * @since 1.0.0
     * @returns {Boolean} Returns true if the platform is mac and false otherwise.
     */
    static isMac(){
        return process.platform === 'darwin';
    }

    /**
     * Check if platform is linux or mac.
     *
     * @method isInux
     * @since 1.0.0
     * @returns {Boolean} Returns true if the platform is linux or mac and false otherwise.
     */
    static isInux(){
        return this.isLinux() || this.isMac();
    }

    /**
     * Get current user directory.
     *
     * @method getUserHome
     * @since 1.0.0
     * @returns {String} Returns true if the platform is windows and false otherwise.
     */
    static getUserHome(){
        return process.env[this.isWindows() ? 'USERPROFILE' : 'HOME'];
    }

    /**
     * Convert the first character of string to lower case.
     *
     * @method lowerFirst
     * @param {String} text The string to convert.
     * @since 1.0.0
     * @returns {String} Returns the converted string.
     */
    static lowerFirst(text){
        return (text.slice(0, 1)).toLowerCase() + text.slice(1);
    }

    /**
     * Convert the first character of string to upper case.
     *
     * @method upperFirst
     * @param {String} text The string to convert.
     * @since 1.0.0
     * @returns {String} Returns the converted string.
     */
    static upperFirst(text){
        return (text.slice(0, 1)).toUpperCase() + text.slice(1);
    }

}