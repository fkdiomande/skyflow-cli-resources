# Skyflow-cli Api-doc

## How to add a new Package ?

If you want to create a new package you need to fullfil the requirements.<br/>
These are simple to understand, first of all you need to respect the architecture.


You need to create a folder with the ***name*** of your package, then in this folder create a sub-folder with the ***version*** of your package.


Then, for a validate package you need to add a file inside your folder, ***name*.config.json**.


Plus you need another file inside your sub-folder, ***name*.yml**.


When it's done, you have create your first package.

## How to fill my files in my package ?

### *name*.config.json

This file is the description file. You put informations like the author, the descriptions, etc. :

```
{
  "name": "The name",
  "description": "The description",
  "author": {
    "name": "Skyflow",
    "email": "contact@skyflow.io",
    "site": "https:\/\/skyflow.io",
    "github": "https:\/\/github.com\/skyflow-io"
  },
  "slug": "The slug",
  "logo": "The logo"
}
```

You can see an example  **<a href="/api/package/django/django.config.json" target="blank">Here</a>**

### *name*.yml

This file containts all the compose that you need inside your compose.

```
{% my_compose1 %}
{% my_compose2:version %}
```
As you can see in this sample of code, you can specifie the version of the compose that you need.

You can see an example  **<a href="/api/package/django/latest/django.yml" target="blank">Here</a>**

### Optional

If you want to use a particular customization of one of the compose that you call inside your ***name*.yml**, you can create a sub-folder inside your **version** folder, call **composes**. Inside this new folder you can create your own custom compose, more informations about how to create your own compose **<a href="/api/compose/README.md" target="blank">Here</a>**

You can see an example  **<a href="/api/package/django/latest/composes" target="blank">Here</a>**

## Contact

You can contact us through **GitHub** or [Slack][slack-url] in our public workspace at 
```
skyflow-cli.slack.com
```

**Thanks you** for your participation in our project.<br/>
The Skyflow CLI team.