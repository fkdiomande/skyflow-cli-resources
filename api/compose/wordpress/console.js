// wordpress

module.exports = {

	questions: [
		{
			message: "Application port",
			name: "port",
			default: "8080",
            validator: (value)=>{
                if(Skyflow.isPortReachable(value)){
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
		},
	],

};
