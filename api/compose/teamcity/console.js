// teamcity

module.exports = {

	questions: [
		{
			message: "Data storage location",
			name: "data_storage_location",
			default: "teamcity/data",
		},
		{
            message: "Logs storage location",
            name: "logs_storage_location",
            default: "teamcity/logs",
        },
		{
			message: "Application port",
			name: "port",
			default: "8080",
            validator: (value)=>{
                if(Skyflow.isPortReachable(value)){
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
		},
	],

};
