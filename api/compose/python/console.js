// python

module.exports = {

	questions: [
		{
			message: "Application name",
			name: "application_name",
			default: "app",
		},
        {
            message: "Entry point",
            name: "entry_point",
            default: "index.py",
        },
	],

};
