// phpunit

module.exports = {

    questions: [
        {
            message: "Tests directory. Relative to the current directory.",
            name: "tests_dir",
            default: "tests",
            validator: (value) => {
                if (!Skyflow.Directory.exists(value)) {
                    return 'Directory not found.'
                }
                return true
            }
        },
    ],

    events: {

        update: {
            after(values){
                const File = Skyflow.File;

                let content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
                content += "\n";
                content += "<phpunit colors=\"true\">\n";
                content += "\n";
                content += "    <testsuites>\n";
                content += "\n";
                content += "        <testsuite name=\"Unit\">\n";
                content += "            <directory>./" + values['tests_dir'] + "</directory>\n";
                content += "        </testsuite>\n";
                content += "\n";
                content += "    </testsuites>\n";
                content += "\n";
                content += "</phpunit>";

                let fileName = "phpunit.xml";

                File.create(fileName, content);
                if(Skyflow.Helper.isInux()){
                    require('fs').chmodSync(fileName, '777');
                }
                Skyflow.Output.success(fileName + " created!");
            },
        },

    }

};
