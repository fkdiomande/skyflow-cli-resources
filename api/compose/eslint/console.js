const {resolve} = require("path");

// eslint

module.exports = {

    questions: [
        {
            message: "Application directory. Relative to the current directory.",
            name: "application_dir",
            default: "src",
        },
    ],


    events: {

        add: {
            after() {
                let eslintDir = resolve(Skyflow.getCurrentDockerDir(), 'eslint');
                Skyflow.Shell.mv(resolve(eslintDir, 'eslintrc.js'), resolve(eslintDir, '.eslintrc.js'));
            },
        },
    },
};
