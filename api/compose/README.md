# Skyflow-cli Api-doc

## How to add a new basic compose ?

If you want to create a new compose you need to fullfil the requirements.<br/>
These are simple to understand, first of all you need to respect the architecture.


You need to create a folder with the ***name*** of your compose, then in this folder create a sub-folder with the ***version*** of your compose.


Then, for a validate compose you need to add 2 files inside your folder, ***name*.config.json** and **console.js**.


Plus you need another file inside your sub-folder, **docker-compose.dist**.


When it's done, you have create your first compose.

## How to fill my files in my compose ?

### *name*.config.json

This file is the description file. You put informations like the author, the descriptions, etc. :

```
{
  "name": "The name",
  "description": "The description",
  "author": {
    "name": "Skyflow",
    "email": "contact@skyflow.io",
    "site": "https:\/\/skyflow.io",
    "github": "https:\/\/github.com\/skyflow-io"
  },
  "slug": "The slug",
  "logo": "The logo"
}
```

You can see an example  **<a href="/api/compose/adminer/adminer.config.json" target="blank">Here</a>**

### console.js

This file containts all the questions you'll see in the console if you try to add your compose into your workspace. This file also containts the default answer to them in case you need a particular configuration.

```
module.exports = {

	questions: [
		{
			message: "Application directory",
			name: "dir",
			default: "../myApp",
		},
		{
			message: "Application port",
			name: "port",
			default: "80",
            validator: (value)=>{
                if(Skyflow.isPortReachable(value)){
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
		},
	],

};
```

You can see an example  **<a href="/api/compose/adminer/console.js" target="blank">Here</a>**

### docker-compose.dist

This file is an equivalent to a docker-compose file. You put here all you're parameters that you need to run you're container. These parameters can be implemented in your ***name*.config.json** and **console.js** files.

```
{{ container_name }}:
        container_name: {{ container_name }}
        image: my_compose:version
        volumes:
            - ../{{ dir }}:/app
```

In this code ``` {{ container_name }} ``` is randomly generate when you update your compose and ``` {{ dir }} ``` is equal to the **dir** that you defined inside your ***name*.config.json**

You can see an example  **<a href="/api/compose/adminer/4.6.2/docker-compose.dist" target="blank">Here</a>**

## How to add a new advanced compose ?

If you need to create a particular compose with a lot of specifications you can add more folder and configuration's files inside your compose than the **Basic version**.

They are an undefined number of variable that you can add and customize. So here is a list of examples that you can use to make your own special compose:

- **<a href="/api/compose/django" target="blank">Django</a>**
- **<a href="/api/compose/laravel" target="blank">Laravel</a>**
- **<a href="/api/compose/prestashop" target="blank">Prestashop</a>**
- **<a href="/api/compose/symfony" target="blank">Symfony</a>**

## Contact

You can contact us through **GitHub** or [Slack][slack-url] in our public workspace at 
```
skyflow-cli.slack.com
```

**Thanks you** for your participation in our project.<br/>
The Skyflow CLI team.