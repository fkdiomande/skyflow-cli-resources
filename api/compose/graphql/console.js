const {resolve} = require("path");

// graphql

module.exports = {

    questions: [
        {
            message: "Host name",
            name: "host",
            default: "localhost",
        },
        {
            message: "Application port",
            name: "port",
            default: "4000",
            validator: (value) => {
                if (Skyflow.isPortReachable(value)) {
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
        },
        {
            message: "Application endpoint",
            name: "endpoint",
            default: "graphql",
        },
        {
            message: "Node environment. Set dev or prod",
            name: "env",
            default: "dev",
            validator: (value) => {
                if (value !== "dev" && value !== "prod") {
                    return "Your environment is not valid. Use dev or prod."
                }

                return true
            }
        },
    ],

    events: {

        add: {
            after() {
                let graphqlDir = resolve(Skyflow.getCurrentDockerDir(), "graphql");
                Skyflow.Shell.mv(resolve(graphqlDir, "graphql-server.js"), resolve(graphqlDir, "..", "..", "graphql-server.js"));
                Skyflow.Shell.mv(resolve(graphqlDir, "schema"), resolve(graphqlDir, "..", "..", "schema"));
            },
        },

    },

    messages: {
        info: [
            "-> For development, see http://localhost:{{ port }}/{{ endpoint }}",
            "-> For production, see https://{{ host }}:{{ port }}/{{ endpoint }}",
        ],
    },

};