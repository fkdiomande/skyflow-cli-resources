// redis

module.exports = {

    questions: [
        {
            message: "Database storage location. Relative to the current directory.",
            name: "database_storage_location",
            default: "./skyflow/database/redis",
        },
        {
            message: "Application port",
            name: "port",
            default: "8080",
            validator: (value)=>{
                if(Skyflow.isPortReachable(value)){
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
        },
    ],

};
