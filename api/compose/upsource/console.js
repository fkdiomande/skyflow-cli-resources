const {resolve} = require("path");

// upsource

module.exports = {

	questions: [
		{
			message: "Data storage location",
			name: "data_storage_location",
			default: "upsource/data",
		},
		{
            message: "Configuration storage location",
            name: "conf_storage_location",
            default: "upsource/conf",
        },
		{
            message: "Logs storage location",
            name: "logs_storage_location",
            default: "upsource/logs",
        },
        {
            message: "Backups storage location",
            name: "backups_storage_location",
            default: "upsource/backups",
        },
		{
			message: "Application port",
			name: "port",
			default: "8080",
            validator: (value)=>{
                if(Skyflow.isPortReachable(value)){
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
		},
	],

    events: {
        up: {
            after(values){
                let tokenFile = resolve('..', values['conf_storage_location'], 'internal/services/configurationWizard/wizard_token.txt');
                let token = null;
                if(Skyflow.File.exists(tokenFile)){
                    token = Skyflow.File.read(tokenFile)
                }
                if(token){
                    Skyflow.Output.writeln("Upsource administrator token : " + token)
                }
            }
        }
    }

};
