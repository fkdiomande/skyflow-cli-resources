// jsdoc

module.exports = {

    questions: [
        {
            message: "Application directory. Relative to the current directory.",
            name: "application_dir",
            default: "src",
        },
        {
            message: "Output directory. Relative to the current directory.",
            name: "output_dir",
            default: "out",
        },
    ],
};
