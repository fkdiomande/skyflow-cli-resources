// elasticsearch

module.exports = {

    questions: [
        {
            message: "Environment password",
            name: "password",
            default: "root",
        },
        {
            message: "Application port",
            name: "port",
            default: "9200",
            validator: (value)=>{
                if(Skyflow.isPortReachable(value)){
                    return "This port is not available."
                }
                Skyflow.addDockerPort(value);
                return true
            }
        },
        {
            message: "Database storage location. Relative to the current directory.",
            name: "database_storage_location",
            default: "../.elasticsearch/my_app",
        },
    ],

};
