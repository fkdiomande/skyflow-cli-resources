# Skyflow-cli Api-doc

## How to custom a module ?

If you want to custom a module you need to modifie it inside the good file and respect the nomenclature.


Each file have it's own nomenclature and it's very important to respect it.

- <a href="/doc/modules/asset.json" target="blank">asset.json</a>
- <a href="/doc/modules/compose.json" target="blank">compose.json</a>
- <a href="/doc/modules/default.json" target="blank">default.json</a>
- <a href="/doc/modules/docker.json" target="blank">docker.json</a>
- <a href="/doc/modules/package.json" target="blank">package.json</a>
- <a href="/doc/modules/symfony.json" target="blank">symfony.json</a>


## Contact

You can contact us through **GitHub** or [Slack][slack-url] in our public workspace at 
```
skyflow-cli.slack.com
```

**Thanks you** for your participation in our project.<br/>
The Skyflow CLI team.