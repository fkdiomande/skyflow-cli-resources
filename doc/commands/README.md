# Skyflow-cli Api-doc

## How to create a new command ?

If you want to create a new command you need to add it inside the good file and respect the nomenclature.


Each file have it's own nomenclature and it's very important to respect it.

- <a href="/doc/commands/asset.json" target="blank">asset.json</a>
- <a href="/doc/commands/compose.json" target="blank">compose.json</a>
- <a href="/doc/commands/default.json" target="blank">default.json</a>
- <a href="/doc/commands/docker.json" target="blank">docker.json</a>
- <a href="/doc/commands/package.json" target="blank">package.json</a>
- <a href="/doc/commands/symfony.json" target="blank">symfony.json</a>


## Contact

You can contact us through **GitHub** or [Slack][slack-url] in our public workspace at 
```
skyflow-cli.slack.com
```

**Thanks you** for your participation in our project.<br/>
The Skyflow CLI team.